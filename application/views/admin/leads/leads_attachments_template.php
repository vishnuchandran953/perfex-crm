<?php defined('BASEPATH') or exit('No direct script access allowed');
$data = '<div class="row">';
foreach($attachments as $attachment) {
    $attachment_url = site_url('download/file/lead_attachment/'.$attachment['id']);
    if(!empty($attachment['external'])){
        $attachment_url = $attachment['external_link'];
    }
    $data .= '<div class="display-block lead-attachment-wrapper">';
    $data .= '<div class="col-md-10">';
    $data .= '<div class="pull-left"><i class="'.get_mime_class($attachment['filetype']).'"></i></div>';
    $data .= '<a href="'.$attachment_url.'" target="_blank">'.$attachment['file_name'].'</a>';
    $data .= '<p class="text-muted">'.$attachment["filetype"].'</p>';
    $data .= '</div>';
    $data .= '<div class="col-md-2 text-right">';
    if($attachment['staffid'] == get_staff_user_id() || is_admin()){
    $data .= '<a href="#" class="text-danger" onclick="delete_lead_attachment(this,'.$attachment['id'].', '.$attachment['rel_id'].'); return false;"><i class="fa fa fa-times"></i></a>';
    }
    $data .= '</div>';
    $data .= '<div class="clearfix"></div><hr/>';
    $data .= '</div>';
    // $data .= '<a href="#"  class="text-danger" onclick="delete_lead_attachment(this,'.$attachment['id'].', '.$attachment['rel_id'].'); return false;">View</a>';
    // $data .= '<embed src="'.$attachment_url.$attachment['file_name'].'" width="800px" height="2100px" />';
    // $data .= '<iframe src="'.$attachment_url.$attachment['file_name'].'" style="width: 100%;height: 100%;border: none;"></iframe>';
    $data .=  '<iframe
    src="https://drive.google.com/viewerng/viewer?embedded=true&url= <?php ?>"
    frameBorder="0"
    scrolling="auto"
    height="100%"
    width="100%"
    ></iframe>';

    
}
$data .= '</div>';
echo $data;
